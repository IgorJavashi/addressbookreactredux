import React, { Component } from 'react';
import Popup from "reactjs-popup";

class EditEntry extends Component {

    constructor(props) {
        super(props);     
        this.state = {
            noviUnos: {}
        } 
    }
 
    handleEdit(e) {
        if (this.refs.name.value === '' || this.refs.surname.value === '' || this.refs.address.value === '' || this.refs.phone.value === '') {
            alert('All fields are required!')
        } else {
           
            this.setState({
                noviUnos: {
                    id: this.props.stavka.id,
                    name: this.refs.name.value,
                    surname: this.refs.surname.value,
                    address: this.refs.address.value,
                    phone: this.refs.phone.value
                }
            }, function () {
                this.props.onEdit(this.state.noviUnos);
            }
            );
        }
        e.preventDefault();
    }

    render() {
        return (
            <div align="center">
                <Popup trigger={<button>Edit Entry</button>} position="bottom center" modal>
                    <div>
                    <form onSubmit={this.handleEdit.bind(this)}>
                            <h3>Edit this entry: </h3>
                            <div>
                                        <label>Name: </label>
                                        <input type="text" ref="name" defaultValue={this.props.stavka.name} />
                                    </div>
                                    <div>
                                        <label>Last Name: </label>
                                        <input type="text" ref="surname" defaultValue={this.props.stavka.surname} />
                                    </div>
                                    <div>
                                        <label>Address: </label>
                                        <input type="text" ref="address" defaultValue={this.props.stavka.address} />
                                    </div>
                                    <div>
                                        <label>Phone: </label>
                                        <input type="text" ref="phone" defaultValue={this.props.stavka.phone} />
                                    </div>
                            <br />
                            <input type="submit" value="Submit" />
                        </form>
                    </div>
                </Popup>
            </div>
        );
    }
}

export default EditEntry;