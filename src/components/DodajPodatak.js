import React, { Component } from 'react';
import uuid from 'uuid';
import Popup from "reactjs-popup";

class DodajPodatak extends Component {

    constructor(props) {
        super(props);
        this.state = {
            noviUnos: {}
        }
    }

    handleSubmit(e) {
        
        if (this.refs.name.value === '' || this.refs.surname.value === '' || this.refs.address.value === '' || this.refs.phone.value === '') {
            alert('All fields are required!')
        } else {
            this.setState({
                noviUnos: {
                    id: uuid.v4(),
                    name: this.refs.name.value,
                    surname: this.refs.surname.value,
                    address: this.refs.address.value,
                    phone: this.refs.phone.value,
                    shown: false
                }
            }, function () {
                this.props.dodajPodatak(this.state.noviUnos);
            }
            );
        }
        e.preventDefault();
    }

    render() {
        return (
            <div align="center">
                <h3>Add new entry</h3>
                <Popup trigger={<button>Add new entry</button>} position="bottom center" modal>
                        <div>
                            <form onSubmit={this.handleSubmit.bind(this)}>
                                <h3> Add new entry: </h3>
                                <div>
                                    <label>Name: </label>
                                    <input type="text" ref="name" />
                                </div>
                                <div>
                                    <label>Last Name: </label>
                                    <input type="text" ref="surname" />
                                </div>
                                <div>
                                    <label>Address: </label>
                                    <input type="text" ref="address" />
                                </div>
                                <div>
                                    <label>Phone: </label>
                                    <input type="text" ref="phone" />
                                </div>
                                <br />
                                <input type="submit" value="Submit"/>
                            </form>
                        </div>
                </Popup>
            </div>
        );
    }
}

export default DodajPodatak;

