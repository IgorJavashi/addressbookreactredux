import React, { Component } from 'react';
import EditEntry from './EditEntry';

class Podatak extends Component {

    constructor(props) {
        super(props);

        this.state = {
            podaci: []
        }
    }

    deleteEntry(id) {
        this.props.onDelete(id);
    }

    editEntry(entry) {
        this.props.onEdit(entry);        
    }

    changeEntry() {
        this.props.changeEntry();
    }

    render() {
        return (
            <div>
                <tr>
                    <td>{this.props.stavka.name} </td>
                    <td>{this.props.stavka.surname}</td>
                    <td>{this.props.stavka.address}</td>
                    <td>{this.props.stavka.phone} </td>
                    <td>
                        <td className="td1">
                            <EditEntry {...this.props} onEdit={this.editEntry.bind(this)} /><br />
                        </td>
                        <td className="td1">
                            <button onClick={this.deleteEntry.bind(this, this.props.stavka.id)}>Delete Entry</button>
                        </td>
                    </td>
                </tr>
            </div>
        );
    } 
}


export default  Podatak;
