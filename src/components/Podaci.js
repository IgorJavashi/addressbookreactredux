import React, { Component } from 'react';
import Podatak from './Podatak';

class Podaci extends Component {

  deleteEntry(id){
    this.props.onDelete(id);
  }

  editEntry(entry){
    this.props.onEdit(entry);
  }

  changeEntry() {
    this.props.onChange();
  }

  render() {
    let pojedinacniPodatak;
    if (this.props.podaci){
        pojedinacniPodatak = this.props.podaci.map(stavka => {
            return (
              <div>
                <Podatak onDelete={this.deleteEntry.bind(this)} key={stavka.name} stavka={stavka} onEdit={this.editEntry.bind(this)} onChange={this.changeEntry.bind(this)}/>    
              </div>
            );
        });
    }
    
    return (
      <div className="Podaci">
        {pojedinacniPodatak}
      </div>
    );
  }
}


export default Podaci;
