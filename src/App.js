import React, { Component } from 'react';
import { Provider } from "react-redux";
import store from "./store";
import { connect } from 'react-redux'
import { createEntry, deleteEntry, updateEntry, search } from './actions/entryActions'
import PropTypes from 'prop-types'
import Podaci from './components/Podaci';
import DodajPodatak from './components/DodajPodatak';

import "./menu.css";
// import SearchInput, { createFilter } from 'react-search-input';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      podaci: [],
      filteredData: [],
      emptyList: []
    }

    this.searchUpdated = this.searchUpdated.bind(this);
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.entries) {
      
      if (document.forms['frm'].search.value !== "" && nextProps.entries.searchResult.length === 0) {
        this.setState({ podaci: this.state.emptyList })
      }
      else {
        this.setState({ podaci: nextProps.entries.searchResult.length > 0 ? nextProps.entries.searchResult : nextProps.entries.entries })
      }
      if (document.forms['frm'].search.value == "") {
        this.setState({ searchResult: [] })
        this.setState({ podaci: nextProps.entries.entries })
      }
    }
  }
 
  componentWillMount() {
    debugger;
    this.setState({podaci: this.props.entries.entries})
    debugger;
  }

  handleDodajPodatak = (podatak) => {
    this.props.createEntry(podatak);
  }

  handleDeleteEntry(id) {
    this.props.deleteEntry(id)
   
  }

  handleEditEntry(newPodatak) {
    this.props.updateEntry(newPodatak);
  }

  searchUpdated(event) {
    this.props.search(event.target.value);
  }

  sortFunction(whatToSort) {
    console.log(whatToSort);

    let podaci = this.state.podaci;
    podaci.sort((a, b) => a[whatToSort].localeCompare(b[whatToSort]));
    this.setState({ podaci: podaci });
  }

   reverseFunction() {
    let podaci = this.state.podaci;
    podaci.reverse();
    this.setState({ podaci: podaci });
  }
  
  render() {
    
    // let podaci = this.state.podaci;

    return (
      <Provider store={store}>
      <div className="App" align="center" >

        <div>
          <h3>Search</h3>
          {/* <SearchInput className='search-input' onChange={this.searchUpdated} /> */}
          <form name="frm">
            <input type="text" name="search" onChange={this.searchUpdated}/>
          </form>
        </div>

        <DodajPodatak {...this.props} dodajPodatak={this.handleDodajPodatak} /> <br />
        
        <h3>Address Book</h3>
        <tr>
        
          <th>Name 
          <i className="fa fa-angle-double-up" onClick={() => this.sortFunction('name')}></i> 
          <i className="fa fa-angle-double-down" onClick={this.reverseFunction.bind(this)}></i>
          </th>
          
          <th>Last Name 
          <i className="fa fa-angle-double-up" onClick={() => this.sortFunction('surname')}></i> 
          <i className="fa fa-angle-double-down" onClick={this.reverseFunction.bind(this)}></i>
          </th>

          <th>Address 
          <i className="fa fa-angle-double-up" onClick={() => this.sortFunction('address')}></i> 
          <i className="fa fa-angle-double-down" onClick={this.reverseFunction.bind(this)}></i>
          </th>

          <th>Phone 
          <i className="fa fa-angle-double-up" onClick={() => this.sortFunction('phone')}></i> 
          <i className="fa fa-angle-double-down" onClick={this.reverseFunction.bind(this)}></i>
          </th>
          <th>Additional Options</th>
        </tr>

        <Podaci {...this.props} podaci={this.state.podaci} onDelete={this.handleDeleteEntry.bind(this)} onEdit={this.handleEditEntry.bind(this)} onChange={this.searchUpdated.bind(this)} />

      </div >
      </Provider>
    );
  }
}

App.propTypes = {
  entries: PropTypes.array.isRequired,
  searchResult: PropTypes.array
}

const mapStateToProps = state => ({
  entries: state.entries,
  searchResult: state.searchResult
})

export default connect(mapStateToProps, { createEntry, deleteEntry, updateEntry, search })(App);