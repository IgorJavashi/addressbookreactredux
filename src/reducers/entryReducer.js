import {
    CREATE_ENTRY,
    UPDATE_ENTRY,
    DELETE_ENTRY,
    // GET_ENTRIES,
    SEARCH
} from '../actions/types'

import { createFilter } from 'react-search-input';

import uuid from 'uuid';


const initialState = {
    entries: [{
            id: uuid.v4(),
            name: 'Pera',
            surname: 'Detlic',
            address: 'Sumarak',
            phone: '018123456789',
            shown: false
        },
        {
            id: uuid.v4(),
            name: 'Mile',
            surname: 'Kitic',
            address: 'Selo',
            phone: '018111111',
            shown: false
        },
        {
            id: uuid.v4(),
            name: 'Djoka',
            surname: 'Djokic',
            address: 'Djokovac',
            phone: '018000000',
            shown: false
        }
    ],
    searchResult: []
}

export default function (state = initialState, action) {
    const { payload } = action;
    switch (action.type) {
        
        case CREATE_ENTRY:
            return { ...state, 
                entries: [...state.entries, payload] };
                
        case DELETE_ENTRY:
            let entries = state.entries.filter((entry) => payload !== entry.id);
            console.log(entries);
            return { ...state, entries };

        case UPDATE_ENTRY:
            entries = state.entries.splice((entry) => payload !== entry.id, 1);
            return { ...state, 
                entries: [...state.entries, payload] };

        case SEARCH: 
            const KEYS_TO_FILTERS = ['name', 'surname', 'address', 'phone'];
            let searchResult = state.entries.filter(createFilter(action.payload, KEYS_TO_FILTERS));
            return { ...state, searchResult };

        default:
            return state
    }
}

