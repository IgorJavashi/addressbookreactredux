import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./store";

import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import App from './App';
import About from './components/About';
import Contact from './components/Contact';


class Main extends Component {
    render() {
      return (
        <Provider store={store}>
        <div className="App" align="center" >
        <HashRouter>
        <div>
          <h1>React Address Book</h1>
          <ul className="header">
            <li><NavLink exact to="/">Home</NavLink></li>
            <li><NavLink to="/Components/about">About</NavLink></li>
            <li><NavLink to="/Components/contact">Contact</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={App}/>
            <Route path="/Components/about" component={About}/>
            <Route path="/Components/contact" component={Contact}/>
          </div>
        </div>
        </HashRouter>
       </div> 
       </Provider>      
      );
    }
  }
   
  export default Main;