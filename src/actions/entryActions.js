import { CREATE_ENTRY, UPDATE_ENTRY, DELETE_ENTRY, GET_ENTRIES, SEARCH } from '../actions/types';

export const createEntry = entryData => ({
    type: CREATE_ENTRY,
    payload: entryData
})

export const deleteEntry = entryData => ({
    type: DELETE_ENTRY,
    payload: entryData
})

export const updateEntry = entryData => ({
    type: UPDATE_ENTRY,
    payload: entryData
})

export const search = searchString => ({
        type: SEARCH,
        payload: searchString
})

export const getEntries = () => ({ type: GET_ENTRIES })