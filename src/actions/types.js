export const CREATE_ENTRY = "CREATE_ENTRY";
export const UPDATE_ENTRY = "UPDATE_ENTRY";
export const DELETE_ENTRY = "DELETE_ENTRY";
export const GET_ENTRIES = "GET_ENTRIES";
export const SEARCH = "SEARCH";